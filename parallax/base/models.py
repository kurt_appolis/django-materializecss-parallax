# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Project(models.Model):
	project_name = models.CharField(max_length=50)
	tagline = models.CharField(max_length=150)
	description = models.TextField(max_length=300)
	contact_details = models.TextField(max_length=300)
	company_bio = models.TextField(max_length=300)

	COLOUR_CHOICES = (
		('red', 'Red'), ('pink', 'Pink'), ('purple', 'Purple'), ('indigo', 'Indigo'), 
		('blue', 'Blue'), ('light-blue', 'Light Blue'), ('cyan', 'Cyan'), ('indigo', 'Indigo'), 
		('blue', 'Blue'), ('light-blue', 'Light Blue'), ('cyan', 'Cyan'), ('green', 'Green'), 
		('light-green', 'Light Green'), ('lime', 'Lime'), ('yellow', 'Yellow'), ('amber', 'Amber'),
		('orange', 'Orange'), ('deep-orange', 'Deep Orange'), ('brown', 'Brown'), ('grey', 'Grey'), 
		('blue-grey', 'Blue Grey'), ('black', 'Black')
		)
	COLOR_ACCENT_DARKEN_CHOICES = (
		(' ', 'None'), 
		('darken-1', 'Darken 1'), ('darken-2', 'Darken 2'), ('darken-3', 'Darken 3'), ('darken-4', 'Darken 4'), 
		('accent-1', 'Accent 1'),('accent-2', 'Accent 2'), ('accent-3', 'Accent 3'), ('accent-4', 'Accent 4')
		)
	
	primary_colour = models.CharField(max_length=50, default="teal", choices=COLOUR_CHOICES)
	primary_accent_colour = models.CharField(max_length=50, default=" ", choices=COLOR_ACCENT_DARKEN_CHOICES)
	
	secondary_colour = models.CharField(max_length=50, default="purple", choices=COLOUR_CHOICES)
	secondary_accent_colour = models.CharField(max_length=50, default=" ", choices=COLOR_ACCENT_DARKEN_CHOICES)

	logo = models.FileField(upload_to="media/imgs/logos/", default="None", blank=True, null=True)
	background_image_1 = models.FileField(upload_to="media/imgs/backgrounds/", default="None", blank=True, null=True)
	background_image_2 = models.FileField(upload_to="media/imgs/backgrounds/", default="None", blank=True, null=True)
	background_image_3 = models.FileField(upload_to="media/imgs/backgrounds/", default="None", blank=True, null=True)
	
	class Meta:
		verbose_name_plural = "Site Detail"