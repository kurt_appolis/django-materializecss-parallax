# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.generic.base import TemplateView

from .models import (
	Project
	)

class LandingPageTemplateView(TemplateView):
	template_name = "base/index.html"
	def get_context_data(self, **kwargs):
		context = super(LandingPageTemplateView, self).get_context_data(**kwargs)
		context['project'] = Project.objects.all().first()
		return context