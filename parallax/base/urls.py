from django.conf.urls import url

from .views import *

app_name = "base"

urlpatterns = [
    url(r'^$', LandingPageTemplateView.as_view(), name="index"),
]
