# A base django project using materializecss parallax template.

# Resources
- http://materializecss.com/getting-started.html
- http://materializecss.com/templates/parallax-template.zip

# Install / Setup

```
#!terminal

> virtualenv ./env

> source ./env/bin/activate

> cd ./parallax

> ./manage.py migrate 

> ./manage.py createsuperuser

> ./manage.py runserver

```

![scaled_1.png](https://bitbucket.org/repo/4p8kazG/images/1729593284-scaled_1.png)

![scaled_2.png](https://bitbucket.org/repo/4p8kazG/images/1293899195-scaled_2.png)